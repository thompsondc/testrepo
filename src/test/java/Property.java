package test.java;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Property {

	public String getVal(String value) {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			String filename = "test/resources/config.properties";
			input = Property.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				System.out.println("Sorry, unable to find " + filename);
				return "Error";
			}

			// load a properties file from class path, inside static method
			prop.load(input);

			// get the property value and print it out
			System.out.println(prop.getProperty(value));
			return prop.getProperty(value);
		} catch (IOException ex) {
			ex.printStackTrace();
			return "Error";
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
					return "Error";
				}
			}
		}
	}

}