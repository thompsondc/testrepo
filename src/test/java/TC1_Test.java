package test.java;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC1_Test {


	private WebDriver driver;
	private String message = "Test PASS!";
	private MessageUtil messageUtil = new MessageUtil(message);
	Property p = new Property();

	@BeforeSuite
	public void setUp() throws Exception {
		
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();
		
	}

	@AfterSuite
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void TC1_Add() {
		try {
			System.out.println("TC - Started running...");

	        //Launch the Online Store Website
			//driver.get("http://www.store.demoqa.com");
			driver.get("http://dev.mgdevops.com:8080/");
			driver.manage().window().maximize();
	        // Print a Log In message to the screen
	        System.out.println("Successfully opened the website");
	        
	        Thread.sleep(2000);
	        
	        // click on language button
			message = "FAIL - 'addLanguage' - not found";
	        if(driver.findElement(By.id(p.getVal("button1"))).isDisplayed())
	        	driver.findElement(By.id(p.getVal("button1"))).click();
	        else {
	        	Assert.fail(new MessageUtil(message).printMessage());
	        }
	        	
	        //Wait for 2 Sec
	        Thread.sleep(2000);
	        
	        // enter language

	        message = "FAIL - 'inEnglish' - not found";
	        if(driver.findElement(By.id("inEnglish")).isDisplayed())
		        driver.findElement(By.id("inEnglish")).sendKeys(p.getVal("input1"));
	        else {
	        	Assert.fail(new MessageUtil(message).printMessage());
	        }
	    	
	        Thread.sleep(2000);
	        
	        // enter language in local
	        message = "FAIL - 'inLocal' - not found";
	        if(driver.findElement(By.id("inLocal")).isDisplayed())
		        driver.findElement(By.id("inLocal")).sendKeys(p.getVal("input2"));
	        else {
	        	Assert.fail(new MessageUtil(message).printMessage());
	        }
	        
	        Thread.sleep(2000);
	        
	        // click add button to enter data into db
	        message = "FAIL - 'addButton' - not found";
	        if(driver.findElement(By.id("addButton")).isDisplayed())
		        driver.findElement(By.id("addButton")).click();
	        else {
	        	Assert.fail(new MessageUtil(message).printMessage());
	        }
	        
	        Thread.sleep(2000);
	        
	        message = "FAIL - 'backButton1' - not found";
	        if(driver.findElement(By.id("backButton1")).isDisplayed())
		        driver.findElement(By.id("backButton1")).click();
	        else {
	        	Assert.fail(new MessageUtil(message).printMessage());
	        }
	        
			//Wait for 5 Sec
			Thread.sleep(2000);
			
			
			message = "Test PASS!";
			Assert.assertEquals(message, new MessageUtil(message).printMessage());

			

		} catch (NoSuchElementException ex) {
			// TODO: handle exception
	        message = "FAIL - 'WebElement' - not found";
        	Assert.fail(new MessageUtil(message).printMessage());
			ex.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
	        message = "FAIL";
        	Assert.fail(new MessageUtil(message).printMessage());
			e.printStackTrace();
		}
	}

	@Test(enabled = false)
	public void testPrintMessage() {
		System.out.println("Inside testPrintMessage()");
		message = "It won't display!!!";
		Assert.assertEquals(message, messageUtil.printMessage());
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("in beforeMethod");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("in afterMethod");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("in beforeClass");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("in afterClass");
	}

	@BeforeTest
	public void beforeTest() {
		System.out.println("in beforeTest");
		try {
			Thread.sleep(6000);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@AfterTest
	public void afterTest() {
		System.out.println("in afterTest");
	}



}
